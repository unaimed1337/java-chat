package UI.RoomsWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by mdccxv on 04.06.2016.
 */
public class RoomsWindow extends JFrame {
    public RoomsWindow()
    {
        createTabbedView();
    }

    private void createTabbedView() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new TabbedPane());
        frame.setSize(250, 500);
        frame.setVisible(true);
    }

    class TabbedPane extends JPanel {
        public TabbedPane()
        {
            prepareGUI();
        }

        private void prepareGUI()
        {
            JTabbedPane jtp = new JTabbedPane();
            jtp.addTab("Rooms", new RoomsPanel());
            jtp.addTab("Users", new UsersPanel());
            add(jtp);
        }
    }
}