package UI;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by mdccxv on 04.06.2016.
 */
public class MainChatWindow extends JFrame {
    public MainChatWindow() {
        prepareGUI();
    }

    private void prepareGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Chat name goes here");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();

        // Set the BoxLayout
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(boxlayout);

        // Set border for the panel
        panel.setBorder(new EmptyBorder(new Insets(20, 5, 20, 5)));
        //panel.setBorder(new EmptyBorder(new Insets(50, 80, 50, 80)));

        // top panel
        JScrollPane top = new JScrollPane();
        top.setBorder(new EmptyBorder(new Insets(20, 20, 20, 20)));
        top.setPreferredSize(new Dimension(200,400));
        top.setLayout(new ScrollPaneLayout());
        top.add(createNewMessage("dsdsdsd","tretertre"));

        // bottom panel
        JPanel bottom = new JPanel();
        bottom.setBorder(new EmptyBorder(new Insets(20, 5, 20, 5)));
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
        JTextField messageField = new JTextField(30);
        JButton sendButton = new JButton("Send");
        bottom.add(messageField);
        bottom.add(sendButton);

        panel.add(top);
        panel.add(bottom);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel createNewMessage(String user, String messageContent)
    {
        JPanel cellPanel = new JPanel();
        cellPanel.setLayout(new BoxLayout(cellPanel, BoxLayout.Y_AXIS));
        cellPanel.add(new JLabel(user));
        cellPanel.add(new JTextArea(messageContent));

        return cellPanel;
    }
}
