package UI;

/**
 * Created by mdccxv on 04.06.2016.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class LoginWindow extends JFrame {

    public LoginWindow() {
        prepareGUI();
    }

    private void prepareGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Choose your nickname");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        JPanel panel = new JPanel();

        // Set the BoxLayout to be X_AXIS: from left to right
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS);

        // Set the Boxayout to be Y_AXIS from top to down
        //BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS);

        panel.setLayout(boxlayout);

        // Set border for the panel
        panel.setBorder(new EmptyBorder(new Insets(150, 200, 150, 200)));
        //panel.setBorder(new EmptyBorder(new Insets(50, 80, 50, 80)));

        JTextField nicknameField = new JTextField(20);
        JButton joinButton = new JButton("Join");

        panel.add(nicknameField);
        panel.add(joinButton);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

}
