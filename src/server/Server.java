package server;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {
    private int port;
    private ServerSocket serverSocket;
    private ConnectionListener connectionListener;
    private MessageDispatcher messageDispatcher;
    private Thread connectionListenerThread;
    private Thread messageDispatcherThread;

    public Server(int port) {
        this.port = port;

        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            System.out.printf("Failed to listen on port %d\n", this.port);
        }

        messageDispatcher = new MessageDispatcher();
        connectionListener = new ConnectionListener(serverSocket, messageDispatcher);
        messageDispatcherThread = new Thread(connectionListener);
        connectionListenerThread = new Thread(messageDispatcher);
    }

    public void run() {
        connectionListenerThread.start();
        messageDispatcherThread.start();
        try {
            connectionListenerThread.join();
            messageDispatcherThread.join();
        } catch (InterruptedException e) {
            System.out.printf("Failed to join threads: %s\n", e.toString());
        }
    }
}
