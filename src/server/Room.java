package server;

import protocol.messages.Message;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Room {
    private List<Client> clients;

    public Room() {
        this.clients = new CopyOnWriteArrayList<>();
    }

    public List<Client> getClients() {
        return clients;
    }

    public boolean hasClient(Client client) {
        return clients.contains(client);
    }

    public void addClient(Client client) {
        if ( ! hasClient(client)) {
            clients.add(client);
        }
    }

    public void removeClient(Client client) {
        if (hasClient(client)) {
            clients.remove(client);
        }
    }

    public void sendToClients(Message message) {
        for (Client client : clients) {
            client.sendMessage(message);
        }
    }
}
