package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionListener implements Runnable {

    private ServerSocket socket;
    private MessageDispatcher messageDispatcher;

    public ConnectionListener(ServerSocket socket, MessageDispatcher messageDispatcher) {
        this.socket = socket;
        this.messageDispatcher = messageDispatcher;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket clientSocket = socket.accept();
                messageDispatcher.addClient(new Client(clientSocket));

                System.out.printf("New connection from %s\n", clientSocket.getInetAddress().getHostAddress());

            } catch (IOException e) {
                System.out.printf("Failed to accept new connection\n");
            }
        }
    }
}
