package server;

import protocol.MessageDecoder;
import protocol.MessageEncoder;
import protocol.messages.Message;

import java.io.*;
import java.net.Socket;
import java.text.ParseException;

public class Client_copy {
    private BufferedReader reader;
    private BufferedWriter writer;
    private String userName;

    public Client_copy(Socket socket) {
        try {
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            System.out.printf("Failed to get inout/output stream of a socket\n");
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean hasMessages() {
        return true;
    }

    public Message readMessage() {
        try {
            if ( ! reader.ready()) {
                return null;
            }

            String messageData = reader.readLine();
            Message message = MessageDecoder.decode(messageData);

            return message;

        } catch (IOException e) {
            System.out.printf("Failed to read message from stream\n");
            return null;
        } catch (ParseException e) {
            System.out.printf("Failed to parse message\n");
            return null;
        }
    }

    public void sendMessage(Message message) {
        try {
            message.setFromUser(userName);
            String messageData = MessageEncoder.encode(message);

            writer.write(messageData);
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            System.out.printf("Failed to write message to stream\n");
        }
    }
}
