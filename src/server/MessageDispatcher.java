package server;

import protocol.messages.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class MessageDispatcher implements Runnable {
    List<Client> clients;
    Map<String, Room> rooms;

    public MessageDispatcher() {
        clients = new CopyOnWriteArrayList<>();
        rooms = new ConcurrentHashMap<>();
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    @Override
    public void run() {
        while (true) {
            for (Client client : clients) {
                Message message = client.readMessage();

                if (message != null) {
                    dispatch(client, message);
                }
            }
        }
    }

    public void dispatch(Client client, Message message) {
        System.out.printf("From %s ::: %s\n", message.getFromUser(), message.getType().toString());

        switch (message.getType()) {
            case UNKNOWN:
                processUnknownMessage(client, message);
                break;
            case USER_LOGIN:
                processUserLogin(client, (UserLoginMessage) message);
                break;
            case POST_TEXT:
                processPostText(client, (PostTextMessage) message);
                break;
            case CREATE_ROOM:
                processCreateRoom(client, (CreateRoomMessage) message);
                break;
            case JOIN_ROOM:
                processJoinRoom(client, (JoinRoomMessage) message);
                break;
            case LEAVE_ROOM:
                processLeaveRoom(client, (LeaveRoomMessage) message);
                break;
            case USER_LOGOUT:
                processUserLogout(client, (UserLogoutMessage) message);
                break;
        }
    }

    private void processUnknownMessage(Client client, Message message) {
        System.out.printf("Unknown message type\n");
    }

    private void processUserLogin(Client client, UserLoginMessage message) {
        if (client.getUserName() != null) {
            System.out.printf("Client already logged in: %s\n", client.getUserName());
            return;
        }

        client.setUserName(message.getFromUser());
    }

    private void processPostText(Client client, PostTextMessage message) {

        if (client.getUserName() == null) {
            System.out.printf("Client does not have a name yet\n");
            return;
        }

        if ( ! rooms.containsKey(message.getRoomName())) {
            System.out.printf("Given room does not exist: %s\n", message.getRoomName());
            return;
        }

        Room room = rooms.get(message.getRoomName());

        if ( ! room.hasClient(client)) {
            System.out.printf("Client %s does not belong to the room %s\n", client.getUserName(), message.getRoomName());
            return;
        }
        room.sendToClients(message);
    }

    private void processCreateRoom(Client client, CreateRoomMessage message) {
        if (client.getUserName() == null) {
            System.out.printf("Client does not have a name yet\n");
            return;
        }

        if (rooms.containsKey(message.getRoomName())) {
            System.out.printf("Given room already exists: %s\n", message.getRoomName());
            return;
        }

        rooms.put(message.getRoomName(), new Room());
    }

    private void processJoinRoom(Client client, JoinRoomMessage message) {
        if (client.getUserName() == null) {
            System.out.printf("Client does not have a name yet\n");
            return;
        }

        if ( ! rooms.containsKey(message.getRoomName())) {
            System.out.printf("Given room does not exist: %s\n", message.getRoomName());
            return;
        }

        Room room = rooms.get(message.getRoomName());

        if (room.hasClient(client)) {
            System.out.printf("Client %s is already a member of the room %s\n", client.getUserName(), message.getRoomName());
            return;
        }

        room.addClient(client);
        room.sendToClients(message);
    }

    private void processLeaveRoom(Client client, LeaveRoomMessage message) {
        if (client.getUserName() == null) {
            System.out.printf("Client does not have a name yet\n");
            return;
        }

        if ( ! rooms.containsKey(message.getRoomName())) {
            System.out.printf("Given room does not exist: %s\n", message.getRoomName());
            return;
        }

        Room room = rooms.get(message.getRoomName());

        if ( ! room.hasClient(client)) {
            System.out.printf("Client %s is not a member of the room %s\n", client.getUserName(), message.getRoomName());
            return;
        }

        room.removeClient(client);
        room.sendToClients(message);
    }

    private void processUserLogout(Client client, UserLogoutMessage message) {
        if (client.getUserName() == null) {
            System.out.printf("Client does not have a name yet\n");
            return;
        }

        LeaveRoomMessage leaveRoomMessage = new LeaveRoomMessage();
        leaveRoomMessage.setFromUser(client.getUserName());

        for (String roomName : rooms.keySet()) {
            Room room = rooms.get(roomName);
            if (room.hasClient(client)) {
                room.removeClient(client);
                leaveRoomMessage.setRoomName(roomName);
                room.sendToClients(leaveRoomMessage);
            }
        }

        clients.remove(client);
    }
}
