package protocol.messages;

public class JoinRoomMessage extends Message {
    private String roomName;

    public JoinRoomMessage() {
        this.type = Type.JOIN_ROOM;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

}
