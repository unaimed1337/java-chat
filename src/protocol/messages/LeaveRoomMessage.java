package protocol.messages;

public class LeaveRoomMessage extends Message {
    String roomName;

    public LeaveRoomMessage() {
        this.type = Type.LEAVE_ROOM;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
