package protocol.messages;

public class UserLogoutMessage extends Message {
    public UserLogoutMessage() {
        this.type = Type.USER_LOGOUT;
    }
}
