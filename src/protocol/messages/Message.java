package protocol.messages;

public class Message {
    public enum Type {
        UNKNOWN,
        USER_LOGIN,
        POST_TEXT,
        CREATE_ROOM,
        JOIN_ROOM,
        LEAVE_ROOM,
        USER_LOGOUT
    }

    protected Type type;
    private String fromUser;
    private String data;

    public Message() {
        this.type = Type.UNKNOWN;
        this.fromUser = new String();
        this.data = new String();
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    }


