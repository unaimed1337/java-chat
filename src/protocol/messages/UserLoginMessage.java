package protocol.messages;

public class UserLoginMessage extends Message {
    public UserLoginMessage() {
        this.type = Type.USER_LOGIN;
    }
}
