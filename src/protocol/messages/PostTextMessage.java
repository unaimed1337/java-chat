package protocol.messages;

public class PostTextMessage extends Message {
    private String text;
    private String roomName;

    public PostTextMessage() {
        this.type = Type.POST_TEXT;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
