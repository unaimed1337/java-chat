package protocol.messages;

public class CreateRoomMessage extends Message {
    private String roomName;

    public CreateRoomMessage() {
        this.type = Type.CREATE_ROOM;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
