package protocol;

import protocol.messages.*;

import java.text.ParseException;
import java.util.Base64;

public class MessageDecoder {

    public static Message decode(String messageData) throws ParseException {
        String[] messageParts = messageData.split(" ");

        if (messageParts.length < 2) {
            throw new ParseException("Incorrect format", 0);
        }

        Base64.Decoder decoder = Base64.getDecoder();

        String userName = new String(decoder.decode(messageParts[0]));
        Message.Type type = Message.Type.valueOf(new String(decoder.decode(messageParts[1])));

        Message message = null;

        switch (type) {
            case USER_LOGIN:
                message = new UserLoginMessage();
                break;
            case POST_TEXT:
                message = new PostTextMessage();

                if (messageParts.length != 4) {
                    throw new ParseException("Incorrect format", 0);
                }
;
                ((PostTextMessage) message).setRoomName(new String(decoder.decode(messageParts[2])));
                ((PostTextMessage) message).setText(new String(decoder.decode(messageParts[3])));
                break;
            case CREATE_ROOM:
                message = new CreateRoomMessage();

                if (messageParts.length != 3) {
                    throw new ParseException("Incorrect format", 0);
                }

                ((CreateRoomMessage) message).setRoomName(new String(decoder.decode(messageParts[2])));
                break;
            case JOIN_ROOM:
                message = new JoinRoomMessage();

                if (messageParts.length != 3) {
                    throw new ParseException("Incorrect format", 0);
                }

                ((JoinRoomMessage) message).setRoomName(new String(decoder.decode(messageParts[2])));
                break;
            case LEAVE_ROOM:
                message = new LeaveRoomMessage();

                if (messageParts.length != 3) {
                    throw new ParseException("Incorrect format", 0);
                }

                ((LeaveRoomMessage) message).setRoomName(new String(decoder.decode(messageParts[2])));
                break;
            case USER_LOGOUT:
                message = new UserLogoutMessage();
                break;
        }

        message.setFromUser(userName);
        message.setType(type);

        return message;
    }
}
