package protocol;

import protocol.messages.*;

import java.util.Base64;

public class MessageEncoder {

    public static String encode(Message message) {
        Base64.Encoder encoder = Base64.getEncoder();

        String userName = encoder.encodeToString(message.getFromUser().getBytes());
        String type = encoder.encodeToString(message.getType().name().getBytes());

        String baseMessageData = String.format("%s %s", userName, type);

        switch (message.getType()) {
            case USER_LOGIN:
                return baseMessageData;
            case POST_TEXT:
                return String.format("%s %s %s", baseMessageData,
                        encoder.encodeToString(((PostTextMessage) message).getRoomName().getBytes()),
                        encoder.encodeToString(((PostTextMessage) message).getText().getBytes()));
            case CREATE_ROOM:
                return String.format("%s %s", baseMessageData,
                        encoder.encodeToString(((CreateRoomMessage) message).getRoomName().getBytes()));
            case JOIN_ROOM:
                return String.format("%s %s", baseMessageData,
                        encoder.encodeToString(((JoinRoomMessage) message).getRoomName().getBytes()));
            case LEAVE_ROOM:
                return String.format("%s %s", baseMessageData,
                        encoder.encodeToString(((LeaveRoomMessage) message).getRoomName().getBytes()));
            case USER_LOGOUT:
                return baseMessageData;
        }

        return baseMessageData;
    }
}
