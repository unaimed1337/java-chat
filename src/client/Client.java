package client;

import protocol.messages.*;
import protocol.MessageDecoder;
import protocol.MessageEncoder;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.text.ParseException;

public class Client {
    SocketAddress address;
    Socket socket;
    BufferedReader reader;
    BufferedWriter writer;
    String userName;

    public Client(String hostName, int port) {
        this.address = new InetSocketAddress(hostName, port);
        this.socket = new Socket();
    }

    public void connect() throws IOException {
        socket.connect(address);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        new ListenFromServer().start();

    }

    public Message readMessage() throws ParseException, IOException {
        if ( ! reader.ready()) {
            return null;
        }

        String messageData = reader.readLine();
        Message message = MessageDecoder.decode(messageData);
        return message;
    }

    public void sendMessage(Message message) throws IOException {
        message.setFromUser(userName);
        String messageData = MessageEncoder.encode(message);
        writer.write(messageData);
        writer.newLine();
        writer.flush();
    }
    public void sendPostMessage(PostTextMessage message) throws IOException {
        message.setFromUser(userName);
        String messageData = MessageEncoder.encode(message);
        writer.write(messageData);
        writer.newLine();
        writer.flush();
    }
    public void run() throws IOException, ParseException {
        BufferedReader stdinReader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            String line = stdinReader.readLine();

            if (line.isEmpty()) {
                continue;
            }

            String[] args = line.split(" ");

            String command = args[0];

            if (command.equalsIgnoreCase("login")) {
                UserLoginMessage msg = new UserLoginMessage();
                userName = args[1];

                sendMessage(msg);
                System.out.printf("ok!\n");

            } else if (command.equalsIgnoreCase("post")) {
                if (args.length != 3) {
                    System.out.printf("Incorrect command: post <room> <text>\n");
                    return;
                }

                PostTextMessage msg = new PostTextMessage();
                msg.setRoomName(args[1]);
                msg.setText(args[2]);

                sendPostMessage(msg);
                System.out.printf("ok!\n");

            } else if (command.equalsIgnoreCase("create")) {
                if (args.length != 2) {
                    System.out.printf("Incorrect command: create <room>\n");
                    return;
                }

                CreateRoomMessage msg = new CreateRoomMessage();
                msg.setRoomName(args[1]);

                sendMessage(msg);
                System.out.printf("ok!\n");

            } else if (command.equalsIgnoreCase("join")) {
                if (args.length != 2) {
                    System.out.printf("Incorrect command: join <room>\n");
                    return;
                }

                JoinRoomMessage msg = new JoinRoomMessage();
                msg.setRoomName(args[1]);

                sendMessage(msg);
                System.out.printf("ok!\n");

            } else if (command.equalsIgnoreCase("leave")) {
                if (args.length != 2) {
                    System.out.printf("Incorrect command: leave <room>\n");
                    return;
                }

                LeaveRoomMessage msg = new LeaveRoomMessage();
                msg.setRoomName(args[1]);

                sendMessage(msg);
                System.out.printf("ok!\n");

            } else {
                System.out.printf("Unrecognised command: %s\n", command);
            }
        }



    }
   // --
    class ListenFromServer extends Thread {
        public void run() {
            Message message = null;

            while(true) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    try {
                     message = readMessage();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (message != null) {
                       //lSystem.out.printf("Received message. Type is: %s\n ", message.getType());
                        if (message.getType().toString().equals("POST_TEXT")){
                            System.out.printf("Message from %s: %s\n ",  message.getFromUser().toString(),((PostTextMessage)message).getText());
                        }
                    }
                }
                catch(IOException e) {
                }


            }
        }
    }
}

