package client;

import java.io.IOException;
import java.text.ParseException;

public class ClientApplication {

    public static void main(String[] args) {
        try {
            Client client = new Client("localhost", 1234);
            client.connect();
            client.run();
        } catch (IOException e) {
            System.out.printf("Failed to connect to server\n");
        } catch (ParseException e) {
            System.out.printf("Failed to parse message\n");
        }
    }
}
